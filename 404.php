<?php
use Timber\Timber;
use Timber\Post;

$context = Timber::get_context();
Timber::render( array( '404.twig' ), $context );