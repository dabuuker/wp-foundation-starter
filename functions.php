<?php

require_once(__DIR__ . '/vendor/autoload.php');

use Timber\Timber;
use Timber\Site;

if ( ! class_exists( 'Timber\Timber' ) ) {
    add_action( 'admin_notices', function() {
        echo '<div class="error"><p>Timber class not found, make sure you have run composer install and required composer\'s autoload.php file</p></div>';
    });

    return;
}

Timber::$dirname = 'templates';
Timber::$locations = array();

if(class_exists('SiteOrigin_Widget')){
    require_once(__DIR__ .'/siteorigin/SiteOrigin_Twig_Widget.php');
}

class StarterSite extends Site {
    function __construct() {

        load_theme_textdomain( 'foundation-starter' );

        /**
         * Make translatable
         */
        load_theme_textdomain( 'foundation-starter' );

        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'html5', array(
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );
        add_theme_support( 'custom-logo', array(
            'width'       => 250,
            'height'      => 250,
            'flex-width'  => true,
        ) );
        add_theme_support( 'customize-selective-refresh-widgets' );
        add_filter( 'timber_context', array( $this, 'add_to_context' ) );
        add_action( 'init', array( $this, 'register_post_types' ) );
        add_action( 'init', array( $this, 'register_taxonomies' ) );
        add_action('wp_enqueue_scripts',array($this,'enqueue_assets'));
        add_action('widgets_init',array($this,'widgets_init'));

        add_filter('siteorigin_widgets_widget_folders',array($this,'siteorigin_widget_folders'));

        parent::__construct();
    }

    function enqueue_assets(){
        wp_enqueue_script('jquery');
        wp_enqueue_script('final',get_theme_file_uri() . '/dist/js/final-min.js',['jquery'],null,true);

        // Initialize foundation JS
        wp_add_inline_script('final','jQuery(document).foundation();');

        wp_enqueue_style('main',get_theme_file_uri() . '/dist/css/main.css',[],null);
    }

    function register_post_types() {
        //this is where you can register custom post types
    }

    function register_taxonomies() {
        //this is where you can register custom taxonomies
    }

    function widgets_init(){
        //this is where you can register widget areas
    }

    function add_to_context( $context ) {
        $context['site'] = $this;
        return $context;
    }

    function siteorigin_widget_folders($folders){
        $folders[] = get_stylesheet_directory() . '/siteorigin/widgets/';

        return $folders;
    }
}

new StarterSite();