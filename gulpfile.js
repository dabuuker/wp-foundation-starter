var gulp = require('gulp');
var exists = require('fs-exists-sync');
var minify = require('gulp-minify');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var replace = require('gulp-replace');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var gulpSequence = require('gulp-sequence');
var autoprefixer = require('gulp-autoprefixer');
var cssjoin = require('gulp-cssjoin');
var file = require('gulp-file');
var livereload = require('gulp-livereload');
var plumber = require('gulp-plumber');

const distPath = './dist/';

gulp.task('sass',function(){
    return gulp.src(['./scss/main.scss'])
        .pipe(cssjoin())
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error',sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(distPath + 'css')).
        pipe(livereload());
});

gulp.task('sassAdmin',function(){
    return gulp.src(['./scss/editor.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error',sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(distPath + 'css'))
});

gulp.task('combineFoundationJsComponents',function(){
    return gulp.src([
        // Foundation Core
        './node_modules/foundation-sites/js/foundation.core.js',

        // Foundation Utilities, comment out or remove if not needed (some components depend on some utilities though)
        './node_modules/foundation-sites/js/foundation.util.mediaQuery.js',
        './node_modules/foundation-sites/js/foundation.util.box.js',
        './node_modules/foundation-sites/js/foundation.util.keyboard.js',
        './node_modules/foundation-sites/js/foundation.util.motion.js',
        './node_modules/foundation-sites/js/foundation.util.nest.js',
        './node_modules/foundation-sites/js/foundation.util.timerAndImageLoader.js',
        './node_modules/foundation-sites/js/foundation.util.touch.js',
        './node_modules/foundation-sites/js/foundation.util.triggers.js',

        // Foundation Components, comment out or remove if not needed (some components depend on utilities
        // and other components though)
        './node_modules/foundation-sites/js/foundation.abide.js',
        './node_modules/foundation-sites/js/foundation.accordion.js',
        './node_modules/foundation-sites/js/foundation.accordionMenu.js',
        './node_modules/foundation-sites/js/foundation.drilldown.js',
        './node_modules/foundation-sites/js/foundation.dropdown.js',
        './node_modules/foundation-sites/js/foundation.dropdownMenu.js',
        './node_modules/foundation-sites/js/foundation.equalizer.js',
        './node_modules/foundation-sites/js/foundation.interchange.js',
        './node_modules/foundation-sites/js/foundation.magellan.js',
        './node_modules/foundation-sites/js/foundation.offcanvas.js',
        './node_modules/foundation-sites/js/foundation.orbit.js',
        './node_modules/foundation-sites/js/foundation.responsiveMenu.js',
        './node_modules/foundation-sites/js/foundation.responsiveToggle.js',
        './node_modules/foundation-sites/js/foundation.reveal.js',
        './node_modules/foundation-sites/js/foundation.slider.js',
        './node_modules/foundation-sites/js/foundation.sticky.js',
        './node_modules/foundation-sites/js/foundation.tabs.js',
        './node_modules/foundation-sites/js/foundation.toggler.js',
        './node_modules/foundation-sites/js/foundation.tooltip.js',
        './node_modules/foundation-sites/js/foundation.zf.responsiveAccordionTabs.js'
    ])
        .pipe(concat('foundation-final.js'))

        .pipe(gulp.dest(distPath + 'js/foundation'));
});

gulp.task('minify',function(){
    return gulp.src([
        distPath + '/js/foundation/foundation-final.js',
        './js/**/*.js'
    ])
        .pipe(plumber())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('final.js'))
        .pipe(minify())
        .pipe(gulp.dest(distPath + 'js'))
        .pipe(livereload());
});

gulp.task('watch',function(){
    livereload.listen();
    var sass = gulp.watch(['./scss/**/*.scss'],['sass','sassAdmin']);
    var js = gulp.watch(['./js/**/*.js'],['minify']);
    var php = gulp.watch(['./*.php','./templates/*.twig']);

    php.on('change',function(event){
       livereload.reload(event);
    });

    sass.on('change',function(event){
        console.log('SCSS File ' + event.path + ' was ' + event.type + ', running tasks...');
    });

    js.on('change',function(event){
        console.log('JS File ' + event.path + ' was ' + event.type + ', running tasks...');
    });

    return console.log('Watching for changes...');
});

gulp.task('buildStyleCss',function(){
    var packageJson = require('./package.json');
    var name = packageJson.name.replace(/-/g,' ');
    name = name.replace(/\b\w/g, l => l.toUpperCase());

    var str = '/*\nTheme Name: ' + name + '\n' +
    'Version:' + packageJson.version + '\n' +
    'Description: ' + packageJson.description + '\n' +
    'Author: ' + packageJson.author + '\n' +
    '*/';

    return file('style.css',str, {src : true}).pipe(gulp.dest('./'));
});

/**
 * First time only tasks
 */
gulp.task('build',gulpSequence('combineFoundationJsComponents','sass','minify','buildStyleCss'));

gulp.task('default',['watch']);