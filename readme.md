## Description

Bare bones Wordpress starter theme with Foundation

## Dependancies
NPM

## Getting Started

Fill in package.json (name, description, author, version) with your
theme details, style.css will be generated from these

Run 'npm install && composer install'

Run 'gulp' to watch sass and js files for changes

## General Usage

Foundation settings should be modified in 'scss/foundation/_settings.scss'

Foundation SCSS components can be added/removed in 'scss/foundation/_foundation.scss'

Foundation JS components can be added/removed in gulpfile.js under
'combineFoundationJsComponents' task

Theme specific styles and imports should go in 'scss/main.scss' after 
foundation import

Any javascript files placed in the js folder will get bundled into
final-min.js

For livereload, Chromes livereload plugin is recommended ( https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en )

For information on how Timber and Twig work, visit https://github.com/timber/timber