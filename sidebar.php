<?php
use Timber\Timber;

$context = Timber::get_context();

Timber::render( array( 'sidebar.twig' ), $context );