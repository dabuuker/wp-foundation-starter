<?php

use Timber\Timber;

//$template_dirs = glob(__DIR__ . '/widgets/**/templates'); var_dump($template_dirs);
Timber::$locations[] = 'siteorigin';

class SiteOrigin_Twig_Widget extends SiteOrigin_Widget {

    function get_html_content($instance,$args,$template_vars,$css_name){
        $template_name = $this->get_template_name($instance) . '.twig';
        $template_file_path = siteorigin_widget_get_plugin_dir_path( $this->id_base ) . $this->get_template_dir( $instance ) . '/' . $template_name;
        $template_file_path = apply_filters('siteorigin_widgets_template_file_' . $this->id_base, $template_file_path, $instance, $this );
        $template_file_path = realpath($template_file_path);

        // Don't accept non Twig files
        if( substr($template_file_path, -5) != '.twig' ) $template_file_path = false;

        $template_html = '';
        if( !empty($template_file_path) && file_exists($template_file_path) ) {
            // Specify exact template path in case different widgets have templates with the same name
            $relative_template_path = 'widgets/' . $this->id_base . '/' . $this->get_template_dir($instance) . '/' . $template_name;
            $template_html = Timber::compile($relative_template_path,$template_vars);
        }

        // This is a legacy, undocumented filter.
        $template_html = apply_filters( 'siteorigin_widgets_template', $template_html, $this->widget_class, $instance, $this );
        $template_html = apply_filters( 'siteorigin_widgets_template_html_' . $this->id_base, $template_html, $instance, $this );

        return $template_html;
    }

    /**
     * @param $instance
     * @return mixed
     *
     * Override this to filter out data that gets passed to template
     */
    function get_template_data($instance){
        return $instance;
    }

    function get_template_dir($instance){
        return 'templates';
    }
}