<?php
/*
Widget Name: Example Widget
Description: An example widget which displays 'Hello world!'.
Author: Tanel Kollamaa
Author URI: http://example.com
Widget URI: http://example.com/example-widget-docs,
Video URI: http://example.com/example-widget-video
*/

class Example_Widget extends SiteOrigin_Twig_Widget{

    function __construct(){
        parent::__construct(
            'example-widget',
            __('Example Widget','foundation-starter'),
            array(
                'description' => __('An Example Widget.', 'foundation-starter')
            ),
            // $control_options array
            array(''),

            //$form_options array
            array(
                'text' => array(
                    'type' => 'text',
                    'label' => __('Hello world! goes here.', 'foundation-starter'),
                    'default' => 'Hello world!'
                ),
            ),
            get_theme_file_path(__FILE__)
        );
    }

    function get_template_name($instance) {
        return 'example-widget';
    }

    function get_template_variables($instance,$args){
        return array(
            'text' => wp_kses_post($instance['text'])
        );
    }
}

siteorigin_widget_register('example-widget', __FILE__, 'Example_Widget');